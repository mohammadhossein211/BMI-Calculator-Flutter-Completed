import 'dart:math';

class CalculatorBrain {
  CalculatorBrain({this.height, this.weight});

  final int height;
  final int weight;

  double _bmi;

  String calculateBMI() {
    _bmi = weight / pow(height / 100, 2);
    return _bmi.toStringAsFixed(1);
  }

  String getResult() {
    if (_bmi >= 25) {
      return 'چاق';
    } else if (_bmi > 18.5) {
      return 'نرمال';
    } else {
      return 'لاغر';
    }
  }

  String getInterpretation() {
    if (_bmi >= 25) {
      return 'وزن شما بیش از حد نرمال است. بهتر است کمی ورزش کنید';
    } else if (_bmi >= 18.5) {
      return 'شما وزن نرمالی دارید. عالیه!';
    } else {
      return 'وزن شما کمتر از حد نرمال است. شما میتوانید کمی بیشتر غذا میل کنید';
    }
  }
}
